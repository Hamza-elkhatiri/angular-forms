import {Component, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  @ViewChild('f') onRegister: NgForm;
  defaultQuestions= "teacher";
  answer = '';
  genders = ['male', 'female'];
  user = {
    username: '',
    email: '',
    secretQuestion: '',
    answer: '',
    gender: ''
  };
  submitted = false;

  suggestUserName() {
    const suggestedName = 'Superuser';
    // this.onRegister.setValue({
    //   userData: {
    //     username: suggestedName,
    //     email: ''
    //   },
    //   secret: 'pet',
    //   questionANswer: '',
    //   gender: 'male'
    // });
    this.onRegister.form.patchValue({
      userData: {
        username: suggestedName
      }
    });
  }

  // onSubmit(form: NgForm) {
  //   console.log(form);
  // }

  onSubmit() {
    this.submitted = true;
    this.user.username = this.onRegister.value.userData.username;
    this.user.email = this.onRegister.value.userData.email;
    this.user.secretQuestion = this.onRegister.value.secret;
    this.user.answer = this.onRegister.value.questionANswer;
    this.user.gender = this.onRegister.value.gender;

    this.onRegister.reset();
  }

}
